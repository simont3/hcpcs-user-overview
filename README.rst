hcpcs-user-overview
===================

Sample script to collect a list of users, their Buckets and Bucket usage from HCP for Cloud Scale (HCP-CS).

Clone the repo:

..  code-block::

    $ git clone https://gitlab.com/simont3/hcpcs-user-overview

Run the script:

..  code-block::

    $ cd hcpcs-user-overview
    $ python3 -m venv .venv      # create a virtual environment
    $ source .venv/bin/activate  # activate it
    $ pip install requests       # install the requests package

    $ python user-overview/user_overview.py -u <user> -p <password> -r <realm> <https://your_hcpcs.domain.com>


License
-------

The MIT License (MIT)

Copyright (c) 2023 Thorsten Simons (sw@snomis.eu)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.