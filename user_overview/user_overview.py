# HCP for Cloud Scale - MAPI usage demo
#
# Copyright (c) 2023 Thorsten Simons (sw@snomis.eu)

import sys
import requests  # package known as "http for humans"
import urllib3   # just used to disable certificate verification warnings
from datetime import datetime

from tools import parseargs, calcbytesize


class Hcpcs:
    """
    A class handling access to HCP-CS.
    """

    def __init__(self, hcpcsuser=None, passwd=None, realm=None, hcpcsfqdn=None):
        """
        :param hcpcsuser:  the user
        :param passwd:     its password
        :param realm:      the security realm
        :param hcpcsfqdn:  HCP-CS fully qualified URI (tex.: https://myhcpcs.example.com), w/o port(!)
        """
        self.user = hcpcsuser
        self.passwd = passwd
        self.realm = realm
        self.hcpcs = hcpcsfqdn
        self.sess = requests.Session()  # initialize a requests session

    def authenticate(self):
        """Acquire an access token and set default headers for the session."""

        # 1st) Acquire an access token
        try:
            r = self.sess.post(f'{self.hcpcs}:8000/auth/oauth', verify=False,
                               data={'grant_type': 'password',
                                     'username': self.user,
                                     'password': self.passwd,
                                     'scope': '*',
                                     'client_secret': 'th_demo',
                                     'client_id': 'th_client',
                                     'realm': self.realm
                                     })
        except Exception as e:
            sys.exit(f'authenticate(bearer) failed with {e}')
        else:
            if r.status_code != 200:
                sys.exit(f'authenticate(bearer) failed with {r.status_code}')
            else:
                t = r.json()
                # update the common headers for the session with the authorization token etc.
                self.sess.headers.update({'Accept': 'application/json',
                                          'Authorization': f'Bearer {t["access_token"]}',
                                          'Content-Type': '*/*',
                                          })

        # 2nd) acquire an XSRF token (required for requests to the Object Storage MAPI, only)
        try:
            r = self.sess.get(f'{self.hcpcs}:9099/mapi/v1/csrf', verify=False,
                              headers={'Accept': '*/*'})
        except Exception as e:
            sys.exit(f'authenticate(xsrf) failed with {e}')
        else:
            if r.status_code != 200:
                sys.exit(f'authenticate(xsrf) failed with {r.status_code}')
            else:
                # update the common headers for the session with the XSRF token
                self.sess.headers.update({'X-XSRF-TOKEN': r.cookies['XSRF-TOKEN']})

    def getusers(self):
        """
        Produce a list of all users known by HCP-CS.

        :returns:  a list of dicts like this: [{'displayName': 'x','id': 'y', 'realmId': 'z'},]
        """
        try:
            r = self.sess.post(f'{self.hcpcs}:9099/mapi/v1/user/list', verify=False,
                               json={'count': 1000})
        except Exception as e:
            sys.exit(f'getusers() failed with {e}')
        else:
            if r.status_code != 200:
                sys.exit(f'getusers() failed with status code {r.status_code}')
            else:
                return r.json()

    def getbuckets(self, userid):
        """
        Retrieve a list of all buckets owened by user.

        :param userid:  the User's Id as returned by getusers()
        :returns:       a list of dicts, like [{'bucketId': 'x', 'bucketName': 'y'},]
        """
        try:
            r = self.sess.post(f'{self.hcpcs}:9099/mapi/v1/user/list_buckets', verify=False,
                               json={'id': userid,
                                     'count': 1000})
        except Exception as e:
            sys.exit(f'getbuckets failed with {e}')
        else:
            if r.status_code != 200:
                sys.exit(f'getbuckets() failed with status code {r.status_code}')
            else:
                return r.json()

    def getusage(self, username):
        """
        Retrieve a users actual usage.

        :param username:  the User's name as returned by getusers()
        :returns:         a dict, like {'bucketname': {'userName': str, 'usageBytes': int, 'totalObjects': int},}
        """

        try:
            # we just want the actual status of usageBytes and totalObjects, so start- and endDateTime
            # are set to "now", in UTC
            r = self.sess.post(f'{self.hcpcs}:9099/mapi/v1/chargeback/system/get_report', verify=False,
                               json={'userName': username,
                                     'startDateTime': datetime.utcnow().isoformat()[:-3]+'Z',
                                     'endDateTime': datetime.utcnow().isoformat()[:-3]+'Z',
                                     'granularity': 'HOURLY',
                                     'header': False})
        except Exception as e:
            sys.exit(f'getbuckets failed with {e}')
        else:
            if r.status_code != 200:
                print(r.text)
                sys.exit(f'getbuckets() failed with status code {r.status_code}')
            else:
                ret = {}
                cblist = r.text.split('\n')[1:]
                if cblist:
                    for rec in cblist:
                        if not rec:
                            continue
                        rec = rec.split(',')
                        ret[rec[2]] = {'userName': rec[1],
                                       'usageBytes': rec[4],
                                       'totalObjects': rec[6]}

                return ret


if __name__ == '__main__':
    urllib3.disable_warnings()  # disable certificate verification warnings
    cred = parseargs()

    hcpcs = Hcpcs(cred.user, cred.password, cred.realm, cred.hcpcs)
    hcpcs.authenticate()
    # we'll get a list like this: [{'displayName': 'x','id': 'y', 'realmId': 'z'},]
    userlist = hcpcs.getusers()

    # prepare by collecting chargeback data for all users
    cb = {}
    for user in userlist:
        # we'll get a dict like this, per user:
        #     {'bucketname': {'userName': str, 'usageBytes': int, 'totalObjects': int},}
        cb.update(hcpcs.getusage(user['displayName']))

    # Now loop through the user list and create an output table

    # table header
    partingline = f'{"-" * 30} + {"-" * 30} + {"-" * 36} + {"-" * 12} + {"-" * 15}'
    print(partingline)
    print(f'{"USER":30} | {"BUCKET":30} | {"ID":36} | {"OBJECTS":>12} | {"CAPACITY USED":>15}')
    print(partingline)

    for user in userlist:
        _username = user["displayName"]
        # request the user's buckets
        # we'll get a list like this: [{'bucketId': 'x', 'bucketName': 'y'},]
        buckets = hcpcs.getbuckets(user['id'])

        if not buckets:
            print(f'{_username:30} | {"*** no buckets ***":30} | {"":36} | {"":12} |')
        else:
            for bucket in buckets:
                # print detail row while mixing in the chargeback data
                print(f'{_username:30} | {bucket["bucketName"]:30} | {bucket["bucketId"]} | '
                      f'{int(cb[bucket["bucketName"]]["totalObjects"]):>12,d} | '
                      f'{calcbytesize(int(cb[bucket["bucketName"]]["usageBytes"])):>15}')
                _username = ''

        print(partingline)
