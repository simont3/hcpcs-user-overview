# HCP for Cloud Scale - MAPI usage demo
#
# Copyright (c) 2023 Thorsten Simons (sw@snomis.eu)
import argparse

def parseargs():
    """
    args - build the argument parser, parse the command line.
    """

    mp = argparse.ArgumentParser()
    mp.add_argument('-u', dest='user', required=True,
                    help='User')
    mp.add_argument('-p', dest='password', required=True,
                    help='Password')
    mp.add_argument('-r', dest='realm', required=True,
                    help='Realm')
    mp.add_argument(dest='hcpcs',
                    help='HCP-CS fully qualified URL (but w/o port)')

    return mp.parse_args()


def calcbytesize(nbytes, kb=False, mb=False, formLang=False, nozero=False):
    """Return a given no. of Bytes in a human-readable format."""
    if nozero and not nbytes:
        return ''

    sz = ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
    szl = ["Byte", "Kibibyte", "Mebibyte", "Gibibyte", "Tebibyte",
           "Pebibyte", "Exbibyte", "Zebibyte", "Yobibyte"]
    i = 0
    neg = False
    if kb:
        nbytes *= 1024  # we got kibibytes!
    if mb:
        nbytes *= 1024*1024  # we got mibibytes!

    if nbytes < 0:
        neg = True
        nbytes *= -1

    while nbytes > 1023:
        nbytes = nbytes / 1024
        i = i + 1

    if neg:
        nbytes *= -1

    if formLang:
        return f"{nbytes:.0f} {szl[i]}" if szl[i] == 'Byte' else f"{nbytes:.2f} {szl[i]}"
    else:
        return f"{nbytes:.0f} {sz[i]}" if sz[i] == 'B' else f"{nbytes:.2f} {sz[i]}"
